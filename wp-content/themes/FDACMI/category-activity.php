<?php get_header(); ?>
<div class='container'>
    <div class='row'>
        <div class="col-xs-7" style="padding-right: 0px; width: 620px !important;">
            <div class="panel_left_top">
                <div class="tils_index green supermarket left_shadow">กิจกรรมเด่น</div>
            </div>
            <div class="panel_left_body left_shadow" id="category_set">                
                <?php $hilight_activity = get_posts(array("category" => 9, "posts_per_page" => 10)); ?>           
                <ul class='hilight_bxslider clearfix'>
                    <?php foreach ($hilight_activity as $post): setup_postdata($post); ?>
                        <li>
                            <?php $cover_img = get_all_size_image(get_post_thumbnail_id()); ?>
                            <p><img src='<?php echo $cover_img['large'] ?>' class='img-responsive'></p>
                            <?php $raws_all_img = get_all_post_image(get_the_ID()); ?>
                            <?php $all_img = group_obj_by_rows($raws_all_img, 6) ?>
                            <?php if (count($raws_all_img)): ?>
                                <div class='thumb_cover'>
                                    <?php foreach ($all_img as $rows): ?>
                                        <div class='row'>
                                            <?php foreach ($row as $img): ?>
                                                <div class='col-xs-2'>
                                                    <a href='<?php echo $img['large'] ?>' class='thumbnail'>
                                                        <img src='<?php echo $img['thumbnail'] ?>' class='img-responsive'>
                                                    </a>
                                                </div>
                                            <?php endforeach; ?>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            <?php endif; ?>
                            <div style="padding: 0px 10px;">
                                <h2 class='green supermarket'><?php the_title() ?></h2>
                                <?php the_excerpt(); ?>
                            </div>
                        </li>
                        <?php wp_reset_postdata(); ?>
                    <?php endforeach; ?>
                </ul>

                <!--Separator-->
                <p>
                    <img src="<?php bloginfo('template_directory'); ?>/img/header2.png" class="img-responsive">
                </p><!--Separator-->
                <?php wp_reset_postdata(); ?>
                <?php $post_obj = array(); ?>
                <?php while (have_posts()): the_post(); ?>
                    <?php $temp['link'] = get_the_permalink(); ?>
                    <?php $temp['img'] = get_all_size_image(get_post_thumbnail_id()); ?>
                    <?php $temp['title'] = get_the_title(); ?>
                    <?php $temp['excerpt'] = mb_substr(get_the_excerpt(), 0, 21, 'utf-8') ?>
                    <?php array_push($post_obj, $temp) ?>
                <?php endwhile; ?>

                <?php $post_obj = group_obj_by_rows($post_obj, 3) ?>

                <?php foreach ($post_obj as $row): ?>
                    <div class="row" style="padding: 0 15px; margin-bottom: 10px;">
                        <?php foreach ($row as $current): ?>
                            <a class="col-xs-4" href="<?php echo $current['link']; ?>" style="text-decoration: none; color: #222;">
                                <div class="thumbnail" style="margin: 0;">
                                    <img src="<?php echo $current['img']['thumbnail']; ?>" class="img-responsive">
                                </div>
                                <h4 class="supermarket margin_bottom_0">
                                    <img src="<?php bloginfo('template_directory'); ?>/img/icon1.png">&nbsp;<?php echo $current['title']; ?>
                                </h4>
                                <p class='font_12 grey'><?php echo $current['excerpt'] ?></p>
                            </a>
                            <?php wp_reset_postdata(); ?>
                        <?php endforeach; ?>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
        <div class="col-xs-5" style="padding-left: 0px; width: 350px !important;">
            <?php get_sidebar(); ?>
        </div>
    </div>
</div>

<script type='text/javascript'>
    $(function () {
        $('.hilight_bxslider').bxSlider({
            minSlides: 0,
            maxSlides: 1,
            slideWidth: 605,
            slideMargin: 50,
            auto: true
        });
    });
</script>

<?php get_footer(); ?>