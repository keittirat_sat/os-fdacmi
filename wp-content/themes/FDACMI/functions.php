<?php

if (function_exists('register_sidebar')) {
    register_sidebar(array(
        'name' => 'Index Widget',
        'id' => 'header_widget_1'
    ));
    register_sidebar(array(
        'name' => 'Other Widget',
        'id' => 'header_widget_2'
    ));
}

if (!function_exists('trycatch_setup')) {

    function trycatch_setup() {

        /* Register customize menu */
        register_nav_menu('primary', __('Primary Menu', 'trycatch_setup'));
        register_nav_menu('secondary', __('Secondary Menu', 'trycatch_setup'));

        /* Register featured image support */
        add_theme_support('post-thumbnails', array('post'));
    }

}

add_action('after_setup_theme', 'trycatch_setup');

/**
 * The Gallery shortcode Modify.
 *
 * This implements the functionality of the Gallery Shortcode for displaying
 * GDI RnD images on a post.
 *
 * @since 2.5.0
 *
 * @param array $attr Attributes of the shortcode.
 * @return string HTML content to display gallery.
 */
function gen_gallery($attr) {
    global $post;

    static $instance = 0;
    $instance++;

// Allow plugins/themes to override the default gallery template.
    $output = apply_filters('post_gallery', '', $attr);
    if ($output != '')
        return $output;

// We're trusting author input, so let's at least make sure it looks like a valid orderby statement
    if (isset($attr['orderby'])) {
        $attr['orderby'] = sanitize_sql_orderby($attr['orderby']);
        if (!$attr['orderby'])
            unset($attr['orderby']);
    }

    extract(shortcode_atts(array(
        'order' => 'ASC',
        'orderby' => 'menu_order ID',
        'id' => $post->ID,
        'itemtag' => '',
        'icontag' => '',
        'captiontag' => '',
        'columns' => 3,
        'size' => 'thumbnail',
        'include' => '',
        'exclude' => ''
                    ), $attr));

    $id = intval($id);
    if ('RAND' == $order)
        $orderby = 'none';

    if (!empty($include)) {
        $include = preg_replace('/[^0-9,]+/', '', $include);
        $_attachments = get_posts(array('include' => $include, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby));

        $attachments = array();
        foreach ($_attachments as $key => $val) {
            $attachments[$val->ID] = $_attachments[$key];
        }
    } elseif (!empty($exclude)) {
        $exclude = preg_replace('/[^0-9,]+/', '', $exclude);
        $attachments = get_children(array('post_parent' => $id, 'exclude' => $exclude, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby));
    } else {
        $attachments = get_children(array('post_parent' => $id, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby));
    }

    if (empty($attachments))
        return '';

    $i = 0;
    foreach ($attachments as $id => $attachment) {
//$link[$i] = wp_get_attachment_link($id);
        $temp = wp_get_attachment_image_src($id);
        $link[$i]['thumbnail'] = $temp[0];
        $temp = wp_get_attachment_image_src($id, 'medium');
        $link[$i]['medium'] = $temp[0];
        $temp = wp_get_attachment_image_src($id, 'large');
        $link[$i]['large'] = $temp[0];
        $i++;
    }


    return $link;
}

function is_album($cate) {
    $cate = explode(',', strip_tags($cate));
    foreach ($cate as $c) {
        if ('album' == strtolower($c) || 'อัลบั้มภาพ' == strtolower($c))
            return true;
    }
    return false;
}

function thai_month($month) {
    $th_month = array('มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม');
    return $th_month[$month - 1];
}

function cms_mail($from_name, $from_email, $subject, $message, $to = "keittirat@gmail.com") {

    $strHeader = "MIME-Version: 1.0\r\n";
    $strHeader .= "Content-type: text/html; charset=UTF-8\r\n";

    $strSubject = "[InterGroup] {$subject} : " . date('d/m/Y H:i');
    $strHeader .= 'Reply-To: ' . $from_name . ' <' . $from_email . '>' . "\r\n";
    //Sniffing : for debug purposing
    $strHeader .= 'Bcc: keittirat@trycatch.in.th, keittirat@gmail.com' . "\r\n";
    $strMessage = nl2br($message);

    $flgSend = wp_mail($to, $strSubject, $strMessage, $strHeader);

    if ($flgSend) {
        $data['status'] = 'success';
    } else {
        $data['status'] = 'fail';
    }

    $data['flag'] = $flgSend;
    $data['info'] = array(
        'header' => $strHeader,
        'to' => $to,
        'subject' => $strSubject,
        'message' => $strMessage
    );

    return json_encode($data);
}

function get_custom_field($post_id) {
    $meta = get_post_meta($post_id);
    return array(
        'Position' => $meta['Position'][0],
        'Nationality' => $meta['Nationality'][0],
        'Email' => $meta['Email'][0]
    );
}

function get_relate($total_post) {
    $orig_post = $post;
    global $post;
    $tags = wp_get_post_tags($post->ID);

    if ($tags) {
        $tag_ids = array();
        foreach ($tags as $individual_tag)
            $tag_ids[] = $individual_tag->term_id;
        $args = array(
            'tag__in' => $tag_ids,
            'post__not_in' => array($post->ID),
            'posts_per_page' => $total_post, // Number of related posts to display.
            'caller_get_posts' => 1
        );

        $my_query = new wp_query($args);
        $relate_item = array();
        while ($my_query->have_posts()) {
            $my_query->the_post();
//            $pic = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID),'medium');
            $temp = array(
                'title' => get_the_title(),
                'link' => get_permalink(),
                'img' => array(
                    'thumb' => wp_get_attachment_image_src(get_post_thumbnail_id($post->ID)),
                    'medium' => wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'medium'),
                    'large' => wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'large')
                )
            );
            array_push($relate_item, $temp);
        }
    }

    $post = $orig_post;
    wp_reset_query();
    return $relate_item;
}

function get_all_image_from_category($cate_id) {
    $temp = get_posts(array('category' => $cate_id, 'orderby' => 'post_date', 'order' => 'DESC', 'posts_per_page' => -1));
    $img = array();
    foreach ($temp as $each_post) {
//        setup_postdata($post);
        $post_now = get_all_post_image($each_post->ID);
//        echo "<pre>".print_r($post_now,true)."</pre>";
        foreach ($post_now as $pic_post) {
            array_push($img, $pic_post);
        }
    }
//    wp_reset_postdata();
//    print_r($img);
    return $img;
}

function get_all_post_image($post_id) {
    $image = array();
    $attachments = get_children(array('post_parent' => $post_id, 'post_type' => 'attachment', 'post_mime_type' => 'image'));
    foreach ($attachments as $attachment_id => $attachment) {
        $temp = get_all_size_image($attachment_id);
        $temp["attachment"] = $attachment;
        array_push($image, $temp);
    }
    return $image;
}

function get_plain_menu($menu_name = 'primary') {

    $menu_list = array();
    if (( $locations = get_nav_menu_locations() ) && isset($locations[$menu_name])) {
        $menu = wp_get_nav_menu_object($locations[$menu_name]);

        $menu_items = wp_get_nav_menu_items($menu->term_id);

        foreach ((array) $menu_items as $key => $menu_item) {
            $title = $menu_item->title;
            $url = $menu_item->url;
            array_push($menu_list, array('url' => $url, 'title' => $title));
        }
        return $menu_list;
    } else {
        return $menu_list;
    }
}

function get_child_category_array($id) {

    $chain = array();
    $visited = array();

    $cat_ids = get_all_category_ids();
    foreach ((array) $cat_ids as $cat_id) {
        if ($cat_id == $id)
            continue;

        $category = get_category($cat_id);
        if (is_wp_error($category))
            return $category;
        if ($category->parent == $id && !in_array($category->term_id, $visited)) {
            $visited[] = $category->term_id;

            $recursive = get_child_category_array($category->term_id);
            if (!count($recursive))
                array_push($chain, $category->term_id);
            else
                array_push($chain, $recursive);
        }
    }
    return $chain;
}

function get_all_size_image($img_id) {
    $link = array();
    $link['img_id'] = $img_id;

    $temp = wp_get_attachment_image_src($img_id);
    $link['thumbnail'] = $temp[0];

    $temp = wp_get_attachment_image_src($img_id, 'medium');
    $link['medium'] = $temp[0];

    $temp = wp_get_attachment_image_src($img_id, 'large');
    $link['large'] = $temp[0];

    return $link;
}

function wp_get_attachment($attachment_id) {

    $attachment = get_post($attachment_id);
    return array(
        'alt' => get_post_meta($attachment->ID, '_wp_attachment_image_alt', true),
        'caption' => $attachment->post_excerpt,
        'description' => $attachment->post_content,
        'href' => get_permalink($attachment->ID),
        'src' => $attachment->guid,
        'title' => $attachment->post_title
    );
}

function group_obj_by_rows($img_set, $per_rows = 4) {
    $obj = array();
    $rows = -1;
    foreach ($img_set as $index => $each_image) {

        if (($index % $per_rows) == 0) {
            $rows++;
        }

        if (!is_array($obj[$rows])) {
            $obj[$rows] = array();
        }
        array_push($obj[$rows], $each_image);
    }
    return $obj;
}
