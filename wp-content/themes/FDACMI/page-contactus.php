﻿<?php session_start(); ?>
<?php if ($_POST): ?>
    <?php
    $data = $_POST;
    $resp["status"] = "fail";
    $session_id = session_id();
    if ($data['c_refkey'] == $session_id) {
        $subject = "[FDACMI] ระบบการฟอร์มติดต่อ - {$data['c_subject']}";
        $to = "fdachiangmai@gmail.com";
        //$to = "keittirat@gmail.com";
		$c_detail = "ชื่อ {$data['c_name']}\nEmail {$data['c_email']}\n----------------\n{$data['c_detail']}";
        $message = $c_detail;
        $mail_header = "Reply-To: {$data['c_email']} <{$data['c_name']}>";
        if ($mail_resp = wp_mail($to, $subject, $message, $mail_header)) {
            $resp['status'] = "success";
        }else{
			$resp['response'] = $mail_resp; 
		}
    }else{
		$resp['response'] = "Else statement";
	}
    echo json_encode($resp);
    ?>
<?php else: ?>
    <?php get_header(); ?>
    <div class='container'>
        <div class='row'>
            <div class="col-xs-7" style="padding-right: 0px; width: 620px !important;">
                <!--Top Panel-->
                <div class="panel_left_top">
                    <div class="tils_index green supermarket left_shadow">Contact info</div>
                </div>
                <div class="panel_left_body left_shadow" style='padding: 20px; background-color: #f2f2f2;'>
                    
                    <div class='row'>
                        <div class='col-xs-7'>
<!--Thai-->
                            <h3 class='light_green supermarket'>กลุ่มงานคุ้มครองผู้บริโภคและเภสัชสาธารณสุข</h3>
                            <h4 class="supermarket">สำนักงานสาธารณสุขจังหวัดเชียงใหม่</h4>
                            <p>เลขที่ 10 ถนนสุเทพ ตำบลสุเทพ อำเภอเมืองเชียงใหม่ จังหวัดเชียงใหม่ 50200</p>
<!--/Thai-->

<!--Eng-->
                            <h3 class='supermarket'>Consumer Protection and Pharmaceutical Department</h3>
                            <h4 class="supermarket">Chiang Mai Provincial Public Health Office</h4>
                            <p>10 Suthep Road, Tumbon Suthep, Mueang Chiang Mai, Chiang Mai, Thailand 50200</p>
<!--/Eng-->
                            <h4 class='supermarket' style="border-top: 1px solid #000; margin-top: 10px; padding-top: 10px;">Tel : 053-894742 , 053-211048 ext 50</h4>
                            <h4 class='supermarket'>Fax 053-894379</h4>
                            <h4 class='supermarket' style="margin-top: 5px;">Email : fdachiangmai@gmail.com</h4>
                        </div>
                        <div class='col-xs-5'>
                            <form method="post" id="contact_form">
                                <label>ชื่อผู้ติดต่อ</label>
                                <div class='form-group'>
                                    <input type='text' name='c_name' class='form-control required-field' placeholder='ชื่อ - นาสกุล' required="">
                                </div>

                                <label>อีเมลล์</label>
                                <div class='form-group'>
                                    <input type='email' name='c_email' class='form-control required-field' placeholder='อีเมลล์' required="">
                                </div>

                                <label>หัวข้อ</label>
                                <div class='form-group'>
                                    <input type='text' name='c_subject' class='form-control required-field' placeholder='หัวข้อ' required="">
                                </div>

                                <label>รายละเอียด</label>
                                <div class='form-group'>
                                    <textarea class='form-control required-field' name='c_detail' placeholder="รายละเอียดในการติดต่อ" style='resize: none;' rows="6" required=""></textarea>
                                </div>

                                <p>
                                    <button type='button' class='btn btn-success' id="submit_btn_fake" data-loading-text="กำลังส่ง...">ส่งฟอร์ม</button>
                                    <button type='submit' class='hidden' id="submit_btn">ส่งฟอร์ม</button>
                                </p>

                                <input type="hidden" name="c_refkey" value="<?php echo session_id(); ?>">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-5" style="padding-left: 0px; width: 350px !important;">
                <div class="panel_right" style="padding-bottom: 20px;">
                    <div class="panel_sidebar_top green">
                        <h3 class="supermarket">แผนที่</h3>
                    </div>
                    <div style='margin: 20px; padding: 10px; border: 1px solid #ccc; border-radius: 4px; '>
                        <div class='map_footer' style='height: 450px; border: none;'></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
		function checkValid(){
			var alert = false;
			$.each($('.required-field'), function(id,ele){ 
				var value = $(ele).val();
				if(value == ""){
					alert = true;
				}
			});
			
			return alert;
		}
		
        $(function() {
			$('#submit_btn_fake').click(function(){
				if(checkValid()){
					swal({title:"คำเตือน", text:"โปรดระบุข้อมูลให้ครบทุกช่อง", type:"warning"});					
				}else{
					$('#submit_btn').trigger('click');
				}
			});
			
            $('#contact_form').ajaxForm({
                beforeSend: function() {
                    $('#submit_btn_fake').button('loading');
                },
                complete: function(xhr) {
                    var resp = xhr.responseText;
                    var json = $.parseJSON(resp.trim());
					console.log(json);
                    if (json.status === "success") {
                        swal({title: "สำเร็จ", text: "ระบบได้ทำการส่งข้อมูลติดต่อเรียบร้อยแล้ว", type: "success"});
                        $('#contact_form').trigger("reset");
                    } else {
                        swal({title: "ล้มเหลว", text: "ระบบไม่สามารถทำการส่งข้อมูลติดต่อได้", type: "error"});
                    }
                    $('#submit_btn_fake').button('reset');
                }
            });
        });
    </script>
    <?php get_footer(); ?>
<?php endif; ?>
