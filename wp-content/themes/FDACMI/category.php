<?php get_header(); ?>
<div class='container'>
    <div class='row'>
        <div class="col-xs-7" style="padding-right: 0px; width: 620px !important;">
            <?php wp_reset_postdata(); ?>
            <div class="panel_left_top">
                <div class="tils_index green supermarket left_shadow"><?php wp_title(); ?></div>
            </div>
            <div class="panel_left_body left_shadow" id="category_set" style="padding: 10px 15px;"> 
                <?php $post_obj = array(); ?>
                <?php while (have_posts()): the_post(); ?>
                    <?php $temp['link'] = get_the_permalink(); ?>
                    <?php $temp['img'] = get_all_size_image(get_post_thumbnail_id()); ?>
                    <?php $temp['title'] = get_the_title(); ?>
                    <?php $temp['excerpt'] = get_the_excerpt(); ?>
                    <?php array_push($post_obj, $temp) ?>
                <?php endwhile; ?>

                <?php foreach ($post_obj as $current): ?>
                    <div class="row">
                        <div class="col-xs-3">
                            <div class="thumbnail" style="margin: 0;">
                                <img src="<?php echo $current['img']['thumbnail'] ? $current['img']['thumbnail'] : get_bloginfo('template_directory') . "/img/default.jpg"; ?>" class="img-responsive">
                            </div>
                        </div>
                        <div class="col-xs-9">
                            <h4 class="supermarket">
                                <a href="<?php echo $current['link'] ?>" style="text-decoration: none;"><img src="<?php bloginfo('template_directory'); ?>/img/icon1.png">&nbsp;<?php echo $current['title']; ?></a>
                            </h4>
                            <p class='font_12 grey'><?php echo $current['excerpt'] ?></p>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
        <div class="col-xs-5" style="padding-left: 0px; width: 350px !important;">
            <?php get_sidebar(); ?>
        </div>
    </div>
</div>

<script type='text/javascript'>
    $(function () {
        $('.hilight_bxslider').bxSlider({
            minSlides: 0,
            maxSlides: 1,
            slideWidth: 605,
            slideMargin: 50,
            auto: true
        });
    });
</script>

<?php get_footer(); ?>