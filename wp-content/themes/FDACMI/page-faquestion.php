<?php get_header(); ?>
<div class='container'>
    <div class='row'>
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="supermarket green" style="margin: 0;">คำถามที่พบบ่อย</h3>
                </div>
                <div class="panel-body general-form-control"><?php the_content(); ?></div>
            </div>            
        </div>
    </div>
</div>
<?php get_footer(); ?>