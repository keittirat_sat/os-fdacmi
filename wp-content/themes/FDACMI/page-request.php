<?php get_header(); ?>
<div class='container'>
    <div class='row'>
        <div class="col-xs-12">
            <div class="plain-form general-form-control"><?php the_content(); ?></div>          
        </div>
    </div>
</div>
<?php get_footer(); ?>