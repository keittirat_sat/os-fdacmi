<!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="<?php bloginfo('template_directory'); ?>/img/favicon.png" type="image/x-icon" />
        <title>
            <?php global $page, $paged; ?>
            <?php wp_title('|', true, 'right'); ?>
            <?php bloginfo('name'); ?>
            <?php $site_description = get_bloginfo('description', 'display'); ?>
            <?php if ($site_description && ( is_home() || is_front_page() )) echo " | $site_description"; ?>
            <?php if ($paged >= 2 || $page >= 2) echo ' | ' . sprintf(__('Page %s', 'twentyeleven'), max($paged, $page)); ?>
        </title>

        <!--CSS Section-->
        <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/bootstrap.min.css">
        <link href='<?php bloginfo('template_directory'); ?>/css/colorbox.css' rel='stylesheet' type='text/css'>    
        <link href='<?php bloginfo('template_directory'); ?>/css/nivoSlider/default/default.css' rel='stylesheet' type='text/css'> 
        <link href='<?php bloginfo('template_directory'); ?>/css/nivoSlider/light/light.css' rel='stylesheet' type='text/css'> 
        <link href='<?php bloginfo('template_directory'); ?>/css/nivoSlider/dark/dark.css' rel='stylesheet' type='text/css'> 
        <link href='<?php bloginfo('template_directory'); ?>/css/nivoSlider/bar/bar.css' rel='stylesheet' type='text/css'>   
        <link href='<?php bloginfo('template_directory'); ?>/css/nivo-slider.css' rel='stylesheet' type='text/css'>    
        <link href='<?php bloginfo('template_directory'); ?>/css/sweet-alert.css' rel='stylesheet' type='text/css'>     
        <link href='<?php bloginfo('template_directory'); ?>/css/datepicker3.css' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('stylesheet_url'); ?>" />

        <!--JS Section-->
        <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery-1.9.1.min.js"></script>
        <script src="<?php bloginfo('template_directory'); ?>/js/bootstrap.min.js"></script>
        <script src="http://maps.google.com/maps/api/js?sensor=true" type="text/javascript"></script>
        <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery.colorbox-min.js"></script>
        <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery.form.js"></script>
        <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery.foggy.min.js"></script>
        <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery.nivo.slider.pack.js"></script>
        <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery.bxslider.min.js"></script>
        <!--<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery.easing.1.3.js"></script>-->
        <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery.fitvids.js"></script>
        <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery.ui.map.js"></script>
        <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/sweet-alert.min.js"></script>
        <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/bootstrap-datepicker.js"></script>
        <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/bootstrap-datepicker.th.js"  charset="UTF-8"></script>


        <?php if (is_singular() && get_option('thread_comments')): ?>
            <?php wp_enqueue_script('comment-reply'); ?>
        <?php endif; ?>

        <?php wp_head(); ?>

        <style type="text/css">
            /*Responsive disabled*/
            body{
                min-width: 970px;
                /*min-width: 1024px;*/
            }

            .container {
                /*                max-width: none !important;
                                min-width: 970px;*/

                /*min-width: 1024px;*/

                width: 970px;
            }

        </style>
    </head>
    <body  <?php body_class(); ?>>
        <!--Header-->
        <div class="container" style="margin-bottom: 10px;">
            <div class="row">
                <div class="col-xs-12">
                    <img src="<?php bloginfo('template_directory'); ?>/img/header_top.jpg" class="img-responsive">
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <div class='slideFrame slider-wrapper theme-default'>
                        <div class='nivoSlider' id='mySlide'>
                            <?php $slide = get_posts(array("category" => 3, "order" => "desc")); ?>
                            <?php foreach ($slide as $post): setup_postdata($post); ?>
                                <?php $each_img = get_all_size_image(get_post_thumbnail_id()) ?>
                                <a href="<?php the_permalink() ?>">
                                    <img src='<?php echo $each_img['large'] ?>' title="<?php the_title(); ?>" alt="<?php the_excerpt() ?>">
                                </a>
                                <?php wp_reset_postdata() ?>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <div class="menu_head supermarket txt_left">
                        <?php wp_nav_menu(array('theme_location' => 'primary', 'container_class' => 'menu-main-menu-container', 'menu_class' => 'menu clearfix')); ?>
                    </div>
                </div>
            </div>
        </div>

        <style>
            .nivo-controlNav{
                display: none;
            }

            .nivoSlider{
                margin: 0px !important;
                box-shadow: none !important;
            }
        </style>
        <script type="text/javascript">
            $(function() {
                $('#mySlide').nivoSlider();
            });
        </script><!--/Header-->

        <?php wp_reset_postdata(); ?>