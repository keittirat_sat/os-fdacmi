<div class="panel_right">
    <div class="panel_sidebar_top green">
        <h3 class="supermarket">ร้องเรียน Online</h3>
    </div>

    <div id="event_calendar"></div>

    <?php $side_id = array(76, 90); ?>
    <?php foreach ($side_id as $each_id): ?>
        <?php $post = get_page($each_id); ?>
        <?php setup_postdata($post); ?>
        <div class="row" style="padding: 15px 12px;">
            <div class="col-xs-4" style="padding: 0px 0px 0px 15px;">
                <img src="<?php bloginfo('template_directory'); ?>/img/<?php echo $each_id ?>.jpg" class="img-responsive">
            </div>
            <div class="col-xs-8">
                <a href="<?php the_permalink(); ?>" class="black">
                    <h4 class="supermarket margin_bottom_0"><?php the_title(); ?></h4>
                </a>
                <p class="font_11 grey"><?php echo mb_substr(get_the_excerpt(), 0, 30); ?></p>
            </div>
        </div>
        <?php wp_reset_postdata(); ?>    
    <?php endforeach; ?>

    <!--Original Post 66 -->
    <div class="row" style="padding: 15px 12px;">
        <div class="col-xs-4" style="padding: 0px 0px 0px 15px;">
            <img src="/wp-content/uploads/2014/12/side2.jpg" class="img-responsive">
        </div>
        <div class="col-xs-8">
            <a href="http://www.chiangmaihealth.com/" class="black">
                <h4 class="supermarket margin_bottom_0">สำนักงานสาธารณสุข เชียงใหม่</h4>
            </a>
            <p class="font_11 grey">เว็บไซต์สำนักงานสาธารณสุข</p>
        </div>
    </div>
    <!--Original Post 66 -->

    <?php $side_id = array(69); ?>
    <?php foreach ($side_id as $each_id): ?>
        <?php $post = get_post($each_id); ?>
        <?php setup_postdata($post); ?>
        <div class="row" style="padding: 15px 12px;">
            <div class="col-xs-4" style="padding: 0px 0px 0px 15px;">
                <?php $img = get_all_size_image(get_post_thumbnail_id()) ?>
                <img src="<?php echo $img['thumbnail'] ?>" class="img-responsive">
            </div>
            <div class="col-xs-8">
                <a href="<?php the_permalink(); ?>" class="black">
                    <h4 class="supermarket margin_bottom_0"><?php the_title(); ?></h4>
                </a>
                <p class="font_11 grey"><?php echo mb_substr(get_the_excerpt(), 0, 30); ?></p>
            </div>
        </div>
        <?php wp_reset_postdata(); ?>
    <?php endforeach; ?>

    <p class="txt_center">
		<a href="http://rxdrugstore.net/drugprice/" target="_blank">
			<img src="<?php bloginfo('template_directory'); ?>/img/stock.jpg" class="img-responsive" style="border-radius: 5px; box-shadow: 0px 0px 10px; display: inline-block; margin: 15px 0px; ">
		</a>
	</p>

    <p class="txt_center">
		<img src="<?php bloginfo('template_directory'); ?>/img/1669.jpg" class="img-responsive" style="border-radius: 5px; box-shadow: 0px 0px 10px; display: inline-block; margin: 15px 0px; ">
	</p>

    <p class="txt_center">
        <a href="http://fdacmi.cmrabbit.com">
            <img src="<?php bloginfo('template_directory'); ?>/img/stuff.jpg" style="display: inline-block; margin: 18px 0px; ">
        </a>
    </p>
	
	<div style="width: 285px; margin: 10px auto 0; padding-bottom:15px;">
	<div class="fb-like-box" data-href="https://www.facebook.com/pages/FDA-Chiangmai/319540424922685?fref=ts" data-width="284" data-colorscheme="light" data-show-faces="true" data-header="false" data-stream="false" data-show-border="false"></div>
	</div>
</div>


<style>
    .datepicker-inline{
        margin: 0 auto;
        width: 308px;
    }

    .datepicker-inline table{
        width: 300px;        
    }
</style>

<script type="text/javascript">
    var post_event;
    var label;
    var additional_timezone;

    function calendarInteractive(date) {
        var unix = Date.parse(date) / 1000;
        var ref = unix + additional_timezone;
        for (var i = 0; i < label.length; i++) {
            if (label[i] == ref) {
                return "red";
            }
        }

        return;
    }

    $(function() {
        post_event = $('#json_data').text();
        post_event = $.parseJSON(post_event);

        label = [];

        $.each(post_event, function(index, data) {
            label.push(index);
        });

        additional_timezone = 60 * 60 * 7;
//        additional_timezone = 0;

        $('#event_calendar').datepicker({
            todayHighlight: true,
            beforeShowDay: function(date) {
                return calendarInteractive(date);
            },
            language: 'th'
        });

        $('#event_calendar').datepicker().on("changeDate", function(e) {
            var unix = Date.parse(e.date) / 1000;
            unix += additional_timezone;

            $.each(post_event, function(x, post) {
                if (x == unix) {
                    var txt = "ข่าวกิจกรรมประจำวันที่ " + $('#event_calendar').find(".active").text();
                    $('#modal_post_title').text(txt);
                    $('#modal_post').modal();
                    var html = "";

                    $.each(post, function(i, each_post) {
                        html += '<div class="row" style="padding: 15px 12px;">';
                        html += '<div class="col-xs-4" style="padding: 0px 0px 0px 15px;">';
                        html += '<img src="' + each_post.thumb + '" class="img-responsive">';
                        html += '</div>';
                        html += '<div class="col-xs-8">'
                        html += '<a href="' + each_post.link + '" class="black">';
                        html += '<h4 class="supermarket margin_bottom_0">' + each_post.post_name + '</h4>';
                        html += '</a>';
                        html += '<p class="font_11 grey">' + each_post.text + '</p>';
                        html += '</div>';
                        html += '</div>';
                    });
                    $('#modal_post_content').html(html);
                    return;
                }
            });

        });
    });
</script>

<?php
$event_post = get_posts(array('posts_per_page' => -1, 'category' => 12, 'post_status' => 'any'));
$order_date = array();
foreach ($event_post as $post) {
	if($post->post_status == 'publish' || $post->post_status == 'future'){
		$unix = strtotime($post->post_date);
		$on_date = mktime(0, 0, 0, date('n', $unix), date('j', $unix), date('Y', $unix));

		if (!array_key_exists($on_date, $order_date)) {
			$order_date[$on_date] = array();
		}

		$temp = array();
		
		setup_postdata($post);
		$img = get_all_size_image(get_post_thumbnail_id());
		$temp['post_name'] = get_the_title();
		$temp['link'] = get_the_permalink();
		$temp['thumb'] = $img['thumbnail'];
		$temp['text'] = get_the_excerpt();
		wp_reset_postdata();

		array_push($order_date[$on_date], $temp);
	}
}
?>
<div class="hidden" id="json_data"><?php echo json_encode($order_date); ?></div>

<div class="modal fade" id="modal_post">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modal_post_title">Modal title</h4>
            </div>
            <div class="modal-body" id="modal_post_content">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
