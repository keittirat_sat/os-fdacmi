<!--footer-->


<?php
$subscriptionForm = \SimpleSubscribe\Developers::getSubscriptionForm();

// check if form was submitted
if ($subscriptionForm->isSubmitted()) {
    // form was submitted
}

// check if form was submitted and was valid = that means, there were no errors
// and subscriber was successfully added / or unsubscribed (depends on which form we have in our object)
if ($subscriptionForm->isSubmitted() && $subscriptionForm->isValid()) {
    // form was submitted - and valid, saved in db show your messages.
}

// check if form was submitted and had erros, those can be non-valid fields,
// subscriber with same address already in system, etc.
if ($subscriptionForm->isSubmitted() && $subscriptionForm->hasErrors()) {
    // dump erorrs
    dump($subscriptionForm->getAllErrors());
    // Note: method $subscriptionForm->getAllErrors() retrieves all errors, you can list them using foreach
    // or save them, do whatever you wish to do.
}
?>

<div class="footer">
    <div class="container">
        <div class="row" style="padding-bottom: 26px;">
            <div class="col-xs-4">
                <form action="" method="post" id="frm-subscriptionFront" novalidate="">
                    <h1 class="white supermarket margin_bottom_0">FDACMI</h1>
                    <h3 class="light_green supermarket margin_bottom_0" style="margin-top: 0px;">Sign up</h3>
                    <p class="font_12">Enter your email address and send your subscription to have  e-newsletters,  delivered directly to your inbox.</p>
                    <h6 class="light_green supermarket" style="margin-top: 0px;">Your Email</h6>
                    <p>
                        <input type="text" name="email" id="frm-email" required data-nette-rules='[{"op":":filled","msg":"E-mail address is requried."},{"op":":email","msg":"Your e-mail address must be valid."}]' value="" placeholder="Subscribe email" class="form-control text" required="">
                    </p>
                    <p class="margin_bottom_0">
                        <button type="submit" name='_submit' class="btn btn-success btn-xs subscribeButton button" value="Subscribe">Subscribe</button>
                        <button type="reset" class="btn btn-success btn-xs">Reset</button>
                    </p>
                    <input type="hidden" name="_form_" value="subscriptionFront">
                </form>
            </div>
            <div class="col-xs-4">
                <h3 class="supermarket light_green">
                    <i class="glyphicon glyphicon-globe"></i>&nbsp;
                    Location
                </h3>
                <div class="map_footer"></div>
            </div>
            <div class="col-xs-4">
                <h3 class="supermarket light_green">
                    Contact info
                </h3>
                <p class="phone_footer_label">
                    <img src="<?php bloginfo('template_directory'); ?>/img/footer_phone.jpg">&nbsp;&nbsp;<span>Call 053 894 742</span>
                </p>
                <h4 class="supermarket light_green">Chiangmai: www.fdachiangmai.com</h4>
                <p class="white font_12">10 Suthep Road, Tumbon Suthep, Mueang Chiang Mai, Chiang Mai, Thailand 50200</p>
                <h4 class="supermarket light_green">Email: fdachiangmai@gmail.com</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 txt_center">
                <img src="http://www.e-zeeinternet.com/count.php?page=1094105&style=default&nbdigits=5" alt="HTML Hit Counter" border="0" ></div>
        </div>
    </div>
</div><!--footer-->

<script type="text/javascript">
    $(function() {
        $('.map_footer').gmap({
            'mapTypeId': google.maps.MapTypeId.STANDARD,
            'zoom': 18,
            'center': '18.790303,98.96454'
        }).bind('init', function(ev, map) {
            $('#map_canvas').gmap('addMarker', {'position': '18.790303,98.96454'});
        });

        $('.colorbox').colorbox({"maxWidth": "90%", "maxHeight": "90%"});
    });
</script>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=194531797284411&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<?php wp_footer(); ?>
</body>
</html>
