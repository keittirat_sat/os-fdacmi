﻿<?php session_start(); ?>
<?php if($_POST): ?>
	<?php

	$data = $_POST;
	$resp["status"] = "fail";
	$session_id = session_id();
//extract($data);
	if ($data['c_refkey'] == $session_id) {

		$subject = "[FDACMI] ระบบการฟอร์มติดต่อ - {$c_subject}";
		$to = "fdachiangmai@gmail.com"; 
		//$to = "keittirat@gmail.com";
		$list_complain = array("ผลิตภัณฑ์สุขภาพ", "การประกอบการ", "การดำเนินงาน อย.", "อื่นๆ");
		$message = "เรื่อง : ".$list_complain[$data['complain_type']] . ($data['complain_type'] == 3 ? " {$data['etc_choice']}\n":"") ." | ".$data['c_subject']." ";
		$message .= "\n ------------------------------- \n";
		$message .= "ชื่อ : {$data['c_name']}"."\n";
		$message .= "อีเมลล์ : {$data['c_email']}";
		$message .=  "\n ------------------------------- \n";
		$message .= "Tel : {$data['c_tel']}";
		$message .= "\n ------------------------------- \n";
		$message .= "หมายเลขบัตรประชาชน : {$data['c_idcard']}"."\n";
		$message .= "ที่อยู่ : {$data['c_address']}";
		$message .= "\n ------------------------------- \n";
		$message .= "หัวเรื่อง: ".$data['c_subject']."\n";
		$message .= $data['c_detail'];

		$mail_header = "Reply-To: {$data['c_name']} <{$data['c_email']}>";
		if($_FILES["myfile"]["name"]){

			$target_dir = "./upload/";
			$target_file = $target_dir . basename($_FILES["myfile"]["name"]); 

			if(move_uploaded_file($_FILES["myfile"]["tmp_name"], $target_file)){
				$mail_resp = wp_mail($to, $subject, $message, $mail_header, array($target_file));			
				unlink($target_file);
			}else{
				$resp['txt'] = 'File upload fail';
			}
		}else{	
			$mail_resp = wp_mail($to, $subject, $message, $mail_header);
		}

		if ($mail_resp) {
			$resp['status'] = "success";
		}else{
			$resp['txt'] = $mail_resp; 
		}
	}else{
		$resp['txt'] = "Mail module fail";
	}
	echo json_encode($resp);
	?>
<?php else: ?>
	<?php get_header(); ?>
	<div class='container'>
		<div class='row'>
			<div class="col-xs-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="supermarket green" style="margin: 0;">แจ้งเรื่องร้องเรียน</h3>
					</div>
					<div class="panel-body general-form-control">
						<div class="alert alert-warning" role="alert">
							<strong>แจ้งสิทธิการใช้งาน</strong> ข้อมูลที่ท่านได้แจ้งเข้ามาทั้งหมดจะถูกเก็บเป็นความลับ
						</div>

						<form class='form form-horizontal' method='post' id='complain_form'  enctype="multipart/form-data" onsubmit="checkValid"> 
							<h2 class='supermarket'>ข้อมูลเกี่ยวกับผู้ร้องเรียน</h2>

							<div class='form-group'>
								<label class='col-xs-2 control-label'>ชื่อ-นามสกุล</label>
								<div class='col-xs-10'>
									<input type='text' name='c_name' placeholder='ชื่อ - นามสกุล' required="required" class='form-control required-field'>
								</div>
							</div>

							<div class='form-group'>
								<label class='col-xs-2 control-label'>ที่อยู่</label>
								<div class='col-xs-10'>
									<textarea name='c_address' placeholder='ที่อยู่' required='required' class='form-control required-field' rows="6"></textarea>
								</div>
							</div>

							<div class='form-group'>
								<label class='col-xs-2 control-label'>หมายเลขโทรศัพท์</label>
								<div class='col-xs-10'>
									<input type='text' name='c_tel' placeholder='หมายเลขโทรศัพท์ที่สามารถติดต่อได้' required='required' class='form-control required-field'>
								</div>
							</div>

							<div class='form-group'>
								<label class='col-xs-2 control-label'>หมายเลขประจำตัวประชาชน</label>
								<div class='col-xs-10'>
									<input type='text' name='c_idcard' placeholder='หมายเลขประจำตัวประชาชน 13 หลัก' required='required' class='form-control required-field' maxlength="13">
								</div>
							</div>

							<div class='form-group'>
								<label class='col-xs-2 control-label'>อีเมลล์</label>
								<div class='col-xs-10'>
									<input type='email' name='c_email' placeholder='อีเมลล์ผู้ติดต่อ' required='required' class='form-control required-field'>
								</div>
							</div>

							<h2 class='supermarket'>ประเภทการร้องเรียน</h2>
							<div class='form-group'>
								<label class='col-xs-2 control-label'>ประเภท</label>
								<div class='col-xs-10'>
									<div class='checkbox'>
										<label><input type='radio' value='0' name='complain_type' checked="">&nbsp;ผลิตภัณฑ์สุขภาพ</label>
									</div>
									<div class='checkbox'>
										<label><input type='radio' value='1' name='complain_type'>&nbsp;การประกอบการ</label>
									</div>
									<div class='checkbox'>
										<label><input type='radio' value='2' name='complain_type'>&nbsp;การดำเนินงาน อย.</label>
									</div>
									<div class='checkbox'>
										<label><input type='radio' value='3' name='complain_type'>&nbsp;อื่นๆ <input type='text' name='etc_choice' placeholder="โปรดระบุ"></label>
									</div>
								</div>
							</div>

							<h2 class='supermarket'>ผู้ร้องเรียนขอบันทึกถ้อยคำด้วยความสัตย์จริง ดังนี้</h2>
							<div class='form-group'>
								<label class='col-xs-2 control-label'>เรื่อง</label>
								<div class='col-xs-10'>
									<input name='c_subject' type='text' placeholder='เรื่องที่ต้องการร้องเรียน' required='' class='form-control required-field'>
								</div>
							</div>
							<div class='form-group'>
								<label class='col-xs-2 control-label'>รายละเอียด</label>
								<div class='col-xs-10'>
									<textarea name='c_detail' placeholder='รายละเอียดเนื้อความ' required='' class='form-control required-field' rows="6"></textarea>
								</div>
							</div>

							<h3>หลักฐานเบื้องต้นที่ได้ยื่นประกอบคำร้อง เช่น ไฟล์ภาพฉลากผลิตภัณฑ์ที่ร้องเรียน, เอกสารแผ่นพับโฆษณา เป็นต้น</h3>
							<div class='form-group'>
								<label class='col-xs-2 control-label'>อัพโหลดเอกสารประกอบ</label>
								<div class='col-xs-10'>
									<input type='file' name='myfile'>
								</div>
							</div>

							<div class='well'>
								<h4>หมายเหตุ</h4>
								<p>* กรณีเป็นการร้องเรียนเกี่ยวกับผลิตภัณฑ์ โปรดระบุ เลขทะเบียน หรือเลขสารบบอาหาร ชื่อผลิตภัณฑ์ รุ่นการผลิต (Lot No) ครั้งที่ผลิต (Batch No) วันที่ผลิต วันหมดอายุ (ถ้ามี)</p>
								<p>** กรุณากรอกข้อมูลให้ครบ เนื่องจากการแจ้งชื่อ / ที่อยู่ / เบอร์โทรศัพท์ จะมีประโยชน์ต่อผู้ร้องเรียน ในกรณีเจ้าหน้าที่ต้องการทราบละเอียดเพิ่มเติม / แจ้งผลดำเนินการให้ทราบ</p>
							</div>

							<div class='well'>
								<ol>
									<li>เป็นการร้องเรียนที่เกิดกับผู้ร้องโดยตรงเฉพาะผลิตภัณฑ์สุขภาพได้แก่ อาหาร , ยา , เครื่องสำอาง , เครื่องมือแพทย์ , วัตถุอันตราย</li>
									<li>เป็นการร้องเรียนการให้บริการทางการแพทย์ การบำบัดรักษา , สถานพยาบาล(คลินิก) ,โรงพยาบาลเอกชน</li>
									<li>เป็นการแจ้งเบาะแส เช่นพบโฆษณาที่อาจเข้าข่ายหลอกลวง , พบการจำหน่ายทางสื่ออิเลคโทรนิค  ,พบการบำบัดรักษาโรคที่ไม่แน่ใจว่าได้รับอนุญาตหรือไม่ ขอให้เจ้าหน้าที่ตรวจสอบ</li>
									<li>ท่านสามารถร้องเรียนโดยตรงได้ที่ กลุ่มงานคุ้มครองผู้บริโภคและเภสัชสาธารณสุข สำนักงานสาธารรสุขจังหวัดเชียงใหม่ โทร 053-894742</li>
								</ol>
							</div>

							<div class='form-group'>
								<div class='col-xs-12 txt_center'>
									<button class='btn btn-success' type='button' data-loading-text="กำลังอัพโหลด..." id="upload_submit_btn_fake">อัพโหลด</button>
									<button class='hidden' type='submit' id="upload_submit_btn">อัพโหลด</button>
									<button class='btn btn-danget' type='reset'>รีเซ็ต</button>
								</div>
							</div>
							<input type="hidden" name="c_refkey" value="<?php echo session_id(); ?>">
						</form>
					</div>
				</div>            
			</div>
		</div>
	</div>
	<script type='text/javascript'>
		function checkValid(){
			var alert = false;
			$.each($('.required-field'), function(id,ele){ 
				var value = $(ele).val();
				if(value == ""){
					alert = true;
				}
			});
			
			return alert;
		}
		
		$(function(){
			$('#upload_submit_btn_fake').click(function(){
				if(checkValid()){
					swal({title:"คำเตือน", text:"โปรดระบุข้อมูลให้ครบทุกช่อง", type:"warning"});					
				}else{
					$('#upload_submit_btn').trigger('click');
				}
			});
			
			$('#complain_form').ajaxForm({
				beforeSend: function(){
					$('#upload_submit_btn_fake').button('loading');
				},
				complete: function(xhr){
					var json = $.parseJSON(xhr.responseText);
					console.log(xhr.responseText);
					if(json.status === "success"){
						swal({title:'สำเร็จ', text:'ระบบได้ทำการส่งข้อความแล้ว', type: 'success', timer: 2000});
						$('#upload_submit_btn_fake').trigger('reset');
					}else{
						swal({title:'ล้มเหลว', text:'ระบบไม่สามารถดำเนินการส่งข้อความ : '+json.txt, type: 'error'});
					}
					$('#upload_submit_btn_fake').button('reset');
				}
			});
		});
	</script>
	<?php get_footer(); ?>
<? endif; ?>