<?php get_header(); ?>

<div class="container" >
	<!--Hilight-->
	<div class="row" style="margin-bottom: 30px; padding: 0px 15px;">
		<?php $hilight = get_posts(array("category" => 8, "posts_per_page" => 4)); ?>
		<?php foreach ($hilight as $post): setup_postdata($post); ?>
			<div class="col-xs-3" style='padding: 0.5px;'>
				<?php $img = get_all_size_image(get_post_thumbnail_id()) ?>
				<a class="hilight_thumb" style="background-image: url('<?php echo $img['medium']; ?>');" href='<?php the_permalink(); ?>'>
					<div class='placement_title white supermarket'>
						<h3 style='margin: 0;'><?php echo the_title(); ?></h3>
						<p style="margin: 0;"><?php echo mb_substr(get_the_excerpt(), 0, 21, "utf-8"); ?></p>
					</div>
				</a>
			</div>
		<?php endforeach; ?>
	</div><!--Hilight-->

	<div class="row">
		<div class="col-xs-7" style="padding-right: 0px; width: 620px !important;">
			<!--Top Panel-->
			<p style="margin: -6px 0px -61px;">
				<img src="<?php bloginfo('template_directory'); ?>/img/header2-2.png" class="img-responsive">
			</p>

			<div class="panel_left_body left_shadow">
				<div id="top_set" style="padding: 70px 25px 10px;">
					<?php $cate_index_top = get_posts(array("category" => 6, "posts_per_page" => 9)); ?>
					<?php $post_obj = group_obj_by_rows($cate_index_top, 3); ?>

					<?php foreach ($post_obj as $each_row): ?>
						<div class="row">
							<?php foreach ($each_row as $post): setup_postdata($post) ?>
								<div class="col-xs-4 news_index_block">
									<?php $img = get_all_size_image(get_post_thumbnail_id()) ?>
									<img src="<?php echo $img['thumbnail'] ?>" class="img-responsive border_grey">
									<a href="<?php the_permalink() ?>" style="color: #333;">
										<h4 class="supermarket">
											<img src="<?php bloginfo('template_directory'); ?>/img/icon1.png">&nbsp;<?php the_title(); ?>
										</h4>
									</a>
									<p><?php echo mb_substr(get_the_excerpt(), 0, 30); ?></p>
								</div>
								<?php wp_reset_postdata(); ?>
							<?php endforeach; ?>
						</div>
					<?php endforeach; ?>
				</div>

				<!--Separator-->
				<p>
					<img src="<?php bloginfo('template_directory'); ?>/img/header2.png" class="img-responsive">
				</p><!--Separator-->

				<div id="bottom_set">
					<div class="bottom_news">
						<ul class="bottom_news_bxslide clearfix">
							<?php $post_obj = get_posts(array("category" => 7, "posts_per_page" => 60)); ?>  
							<?php $post_obj = group_obj_by_rows($post_obj, 3) ?>
							<?php foreach ($post_obj as $each_row): ?>
								<li class="display_index" style="min-height: 250px;">
									<div class="row news_rows">
										<?php foreach ($each_row as $post):setup_postdata($post); ?>
											<div class="col-xs-4 news_index_block">
												<?php $img = get_all_size_image(get_post_thumbnail_id()) ?>
												<p style="margin: 0; padding: 0 0 5px;"><img src="<?php echo $img['thumbnail'] ?>" class="img-responsive border_grey"></p>

												<p class="supermarket" style="font-size: 16px; padding-left: 0px; margin: 0;">
													<a href="<?php the_permalink() ?>" style="color: #333;">
														<?php the_title(); ?>
													</a>
												</p>
												<p style="padding: 0 0 10px; border-bottom: 1px solid #ccc; margin-bottom: 5px;"><?php echo mb_substr(get_the_excerpt(), 0, 90); ?></p>
												<p class="supermarket">
													<a href="<?php the_permalink() ?>" style="color: #c3c3c3; font-size: 12px;">อ่านต่อ <img src="<?php bloginfo('template_directory'); ?>/img/bullet_mini.jpg"></a>
												</p>
											</div>
											<?php wp_reset_postdata(); ?>
										<?php endforeach; ?>
									</div>
								</li>
							<?php endforeach; ?>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="col-xs-5" style="padding-left: 0px; width: 350px !important;">
			<?php get_sidebar(); ?>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(function () {
		$('.post_slide_box').bxSlider({
			minSlides: 0,
			maxSlides: 1,
			slideWidth: 605,
			slideMargin: 50,
			auto: true
		});

		$('.bottom_news_bxslide').bxSlider({
			minSlides: 0,
			maxSlides: 1,
			slideWidth: 605,
			slideMargin: 50
		});
	});
</script>

<?php get_footer(); ?>
