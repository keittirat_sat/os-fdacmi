<?php get_header(); ?>
<div class='container'>
    <div class='row'>
        <div class="col-xs-7" style="padding-right: 0px; width: 620px !important;">
            <!--Top Panel-->
            <div class="panel_left_top">
                <div class="tils_index green supermarket left_shadow"><?php the_title(); ?></div>
            </div>
            <div class="panel_left_body left_shadow format-control" style='padding: 15px;'>
                <?php the_content(); ?>
            </div>
        </div>
        <div class="col-xs-5" style="padding-left: 0px; width: 350px !important;">
            <?php get_sidebar(); ?>
        </div>
    </div>
</div>
<?php get_footer(); ?>